FROM node:lts-buster
RUN npm i hexo-cli -g
RUN hexo init public
WORKDIR public
RUN npm install
RUN sed -i 's/Hexo/В нем отображаются дата\/время выпуска и коммит выката/g' themes/landscape/layout/_partial/footer.ejs
RUN sed -i 's/http:\/\/hexo.io\//ver.html/g' themes/landscape/layout/_partial/footer.ejs
RUN hexo generate
COPY ver.html public/
RUN ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
ENTRYPOINT ["hexo","server","-p","80"]
